package compiler;

import compiler.error.Compare;
import compiler.gen.MiniJavaBaseListener;
import compiler.gen.MiniJavaParser;
import compiler.symbolTable.SymbolTable;
import compiler.symbolTable.symbolTableItem.ClassSymbolTableItem;
import compiler.symbolTable.symbolTableItem.InterfaceSymbolTableItem;
import compiler.symbolTable.symbolTableItem.MethodSymbolTableItem;
import compiler.types.Type;
import compiler.types.arrayType.ArrayType;
import compiler.types.singleType.BoolType;
import compiler.types.singleType.IntType;
import org.abego.treelayout.internal.util.java.lang.string.StringUtil;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import compiler.error.Error;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class FindErrors extends MiniJavaBaseListener {
    SymbolTable currentSymbolTable;
    public static PriorityQueue<Error> errors = new PriorityQueue<>(new Compare());

    @Override
    public void enterProgram(MiniJavaParser.ProgramContext ctx) {
        // get symbolTable: (name_[lineNumber]_[column])  for example: program_1_0
        currentSymbolTable = SymbolTable.getSymbolTableByKey("program_"+ctx.start.getLine() + "_0");
        System.out.println(currentSymbolTable);
        // TODO
    }

    @Override
    public void exitProgram(MiniJavaParser.ProgramContext ctx) {
        currentSymbolTable = currentSymbolTable.getPreSymbolTable();
        for(int i=0; i<errors.size(); i++){
            System.out.println(errors.peek());
        }
        // TODO
    }

    @Override
    public void enterMainClass(MiniJavaParser.MainClassContext ctx) {
        currentSymbolTable = SymbolTable.getSymbolTableByKey(ctx.className.getText()+"_"+ctx.start.getLine() + "_"+ctx.className.getCharPositionInLine());
        System.out.println(currentSymbolTable);
        // TODO
    }

    @Override
    public void exitMainClass(MiniJavaParser.MainClassContext ctx) {
        currentSymbolTable = currentSymbolTable.getPreSymbolTable();
        // TODO
    }

    @Override
    public void enterClassDeclaration(MiniJavaParser.ClassDeclarationContext ctx) {
        currentSymbolTable = SymbolTable.getSymbolTableByKey(ctx.className.getText()+"_"+ctx.start.getLine() + "_"+ctx.className.getCharPositionInLine());
        System.out.println(currentSymbolTable);

        List<InterfaceSymbolTableItem> interfaces = ((ClassSymbolTableItem) currentSymbolTable).parents;
        //ClassSymbolTableItem classItem = (ClassSymbolTableItem) currentSymbolTable.get("class_"+ctx.className.getText());
        int line = ctx.start.getLine();
        int column = ctx.className.getCharPositionInLine();
        String parentClasses = ctx.className.getText();

        if ((ctx.parentName != null) && (!ctx.parentName.getText().equals("String"))){
            parentClasses = parentClasses +" -> " + ctx.parentName.getText();
            if (currentSymbolTable.get("class_"+ctx.parentName.getText()) != null){
                ClassSymbolTableItem currClass = (ClassSymbolTableItem)currentSymbolTable.get("class_" + ctx.parentName.getText());
                parentClasses = parentClasses + " -> " + currClass.parent.getKey();
                currClass = currClass.parent;
                while (!currClass.equals("Object")){
                    currClass = (ClassSymbolTableItem)currentSymbolTable.get("class_" + ctx.parentName.getText());
                    parentClasses = parentClasses + " -> " + currClass.parent.getKey();
                    if (currClass.parent.equals(ctx.className.getText())){
                        FindErrors.errors.add(new Error(410,line,column,"invalid inheritance "+parentClasses));
                    }

                }
            }
            else{
                errors.add(new Error(105,line,column,"cannot find class " + ctx.parentName.getText()));
            }
        }


        // TODO
    }

    @Override
    public void exitClassDeclaration(MiniJavaParser.ClassDeclarationContext ctx) {
        currentSymbolTable = currentSymbolTable.getPreSymbolTable();
        // TODO
    }

    @Override
    public void enterInterfaceDeclaration(MiniJavaParser.InterfaceDeclarationContext ctx) {
        currentSymbolTable = SymbolTable.getSymbolTableByKey(ctx.interfaceName.getText()+"_"+ctx.start.getLine() + "_"+ctx.interfaceName.getCharPositionInLine());
        System.out.println(currentSymbolTable);
        // TODO
    }

    @Override
    public void exitInterfaceDeclaration(MiniJavaParser.InterfaceDeclarationContext ctx) {
        currentSymbolTable = currentSymbolTable.getPreSymbolTable();
        // TODO
    }

    @Override
    public void enterInterfaceMethodDeclaration(MiniJavaParser.InterfaceMethodDeclarationContext ctx) {
        int line = ctx.start.getLine();
        int column = ctx.start.getCharPositionInLine();
        currentSymbolTable = SymbolTable.getSymbolTableByKey(ctx.methodName.getText() + "_" + ctx.start.getLine() + "_" + ctx.methodName.getCharPositionInLine());
        System.out.println(ctx.returnType().getText());
//        if(ctx.methodBody().expression() != null)
//            System.out.println(ctx.methodBody().expression());
        System.out.println(currentSymbolTable);

        SymbolTable preSymbolTable = currentSymbolTable.getPreSymbolTable();
        MethodSymbolTableItem declared = (MethodSymbolTableItem) preSymbolTable.get("method_"+ctx.methodName.getText());
        if (!declared.getReturnType().equals(ctx.returnType())){
            errors.add(new Error(210,line,column,"ReturnType of this method must be "+declared.getReturnType().toString()));
        }
        if (!declared.getArgumentsTypes().equals(ctx.parameterList())){
            errors.add(new Error(220,line,column," Mismatch arguments."));
        }
        // TODO
    }

    @Override
    public void exitInterfaceMethodDeclaration(MiniJavaParser.InterfaceMethodDeclarationContext ctx) {
        // TODO
    }

    @Override
    public void enterFieldDeclaration(MiniJavaParser.FieldDeclarationContext ctx) {
        // TODO
        int line = ctx.start.getLine();
        int column = ctx.fieldName.getCharPositionInLine();

        if (ctx.type().Identifier() != null){
            if (ctx.type().Identifier().getText().equals("String")){
                if (currentSymbolTable.get("class_"+ctx.type().Identifier().getText()) == null){
                    errors.add(new Error(105,line,column,"cannot find class" + ctx.type().Identifier().getText()));
                }
            }
        }
        if (ctx.EQ().getText() != null){

        }


    }

    @Override
    public void exitFieldDeclaration(MiniJavaParser.FieldDeclarationContext ctx) {
        // TODO
    }

    @Override
    public void enterLocalDeclaration(MiniJavaParser.LocalDeclarationContext ctx) {
        // TODO

    }

    @Override
    public void exitLocalDeclaration(MiniJavaParser.LocalDeclarationContext ctx) {
        // TODO
    }
    @Override
    public void enterMethodDeclaration(MiniJavaParser.MethodDeclarationContext ctx) {
        int line = ctx.start.getLine();
        int column = ctx.start.getCharPositionInLine();
        currentSymbolTable = SymbolTable.getSymbolTableByKey(ctx.methodName.getText() + "_" + ctx.start.getLine() + "_" + ctx.methodName.getCharPositionInLine());
        System.out.println(ctx.returnType().getText());
        if(ctx.methodBody().expression() != null)
            System.out.println(ctx.methodBody().expression());
        System.out.println(currentSymbolTable);

        SymbolTable preSymbolTable = currentSymbolTable.getPreSymbolTable();
        MethodSymbolTableItem declared = (MethodSymbolTableItem) preSymbolTable.get("method_"+ctx.methodName.getText());
        if (!declared.getReturnType().equals(ctx.returnType())){
            errors.add(new Error(210,line,column,"ReturnType of this method must be "+declared.getReturnType().toString()));
        }
        if (!declared.getArgumentsTypes().equals(ctx.parameterList())){
            errors.add(new Error(220,line,column," Mismatch arguments."));
        }

        boolean found = false;
        String override = ctx.Override().getText();
        if (override != null){
            SymbolTable extendedClass = ((ClassSymbolTableItem)currentSymbolTable.getPreSymbolTable()).parent;
            if (extendedClass.get(ctx.methodName.getText()) != null){
                found = true;
                MethodSymbolTableItem parentMethod = (MethodSymbolTableItem) extendedClass.get("method_"+ctx.methodName.getText());
                if (parentMethod != null && !parentMethod.getReturnType().equals(ctx.returnType().getText())){
                    errors.add(new Error(240,line,column,"the return type of the overriding method must be the same as that of the overridden method"));
                }
                if (parentMethod != null && !parentMethod.getArgumentsTypes().equals(ctx.parameterList())){
                    errors.add(new Error(250,line,column,"the parameters of the overriding method must be the same as that of the overridden method"));
                }
                if (parentMethod.getAccessModifier().equals("private") && ctx.accessModifier().getText().equals("public")){
                    errors.add(new Error(320,line,column,"the access level cannot be more restrictive than the overridden method's access level"));
                }
            }
            else{
                List<InterfaceSymbolTableItem> interfaceParents = ((ClassSymbolTableItem)currentSymbolTable.getPreSymbolTable()).parents;
                if (!interfaceParents.isEmpty()){
                    for (InterfaceSymbolTableItem interfaceParent : interfaceParents){
                        SymbolTable currSymbolTable = interfaceParent.getSymbolTable();
                        if (currSymbolTable.get("method_"+ctx.methodName.getText()) != null){
                            found = true;
                            MethodSymbolTableItem parentMethod = (MethodSymbolTableItem) currSymbolTable.get("method_"+ctx.methodName.getText());
                            if (parentMethod != null && !parentMethod.getReturnType().equals(ctx.returnType().getText())){
                                errors.add(new Error(240,line,column,"the return type of the overriding method must be the same as that of the overridden method"));
                            }
                            if (parentMethod != null && !parentMethod.getArgumentsTypes().equals(ctx.parameterList())){
                                errors.add(new Error(250,line,column,"the parameters of the overriding method must be the same as that of the overridden method"));
                            }
                            if (parentMethod.getAccessModifier().equals("private") && ctx.accessModifier().getText().equals("public")){
                                errors.add(new Error(320,line,column,"the access level cannot be more restrictive than the overridden method's access level"));
                            }

                        }

                    }
                }
            }
            if (!found){
                errors.add(new Error(440,line,column,"method does not override method from its superclass."));
            }
        }
    }

    @Override
    public void exitMethodDeclaration(MiniJavaParser.MethodDeclarationContext ctx) {
        currentSymbolTable = currentSymbolTable.getPreSymbolTable();
        // TODO
    }

    @Override
    public void enterParameterList(MiniJavaParser.ParameterListContext ctx) {
        // TODO
    }

    @Override
    public void exitParameterList(MiniJavaParser.ParameterListContext ctx) {
        // TODO
    }

    @Override
    public void enterParameter(MiniJavaParser.ParameterContext ctx) {
        // TODO
    }

    @Override
    public void exitParameter(MiniJavaParser.ParameterContext ctx) {
        // TODO
    }

    @Override
    public void enterMethodBody(MiniJavaParser.MethodBodyContext ctx) {
        SymbolTable parentSymbolTable = currentSymbolTable;
        Type returnType = ((MethodSymbolTableItem)parentSymbolTable).getReturnType();
        if (!getExpressionType(ctx.expression().getText()).equals(returnType)){
            errors.add(new Error(210,ctx.start.getLine(),ctx.start.getCharPositionInLine(),"ReturnType of this method must be" + returnType.toString()));
        }


    }

    @Override
    public void exitMethodBody(MiniJavaParser.MethodBodyContext ctx) {
        // TODO
    }

    @Override
    public void enterType(MiniJavaParser.TypeContext ctx) {
        // TODO
    }

    @Override
    public void exitType(MiniJavaParser.TypeContext ctx) {
        // TODO
    }

    @Override
    public void enterBooleanType(MiniJavaParser.BooleanTypeContext ctx) {
        // TODO
    }

    @Override
    public void exitBooleanType(MiniJavaParser.BooleanTypeContext ctx) {
        // TODO
    }

    @Override
    public void enterIntType(MiniJavaParser.IntTypeContext ctx) {
        // TODO
    }

    @Override
    public void exitIntType(MiniJavaParser.IntTypeContext ctx) {
        // TODO
    }

    @Override
    public void enterReturnType(MiniJavaParser.ReturnTypeContext ctx) {
        // TODO
    }

    @Override
    public void exitReturnType(MiniJavaParser.ReturnTypeContext ctx) {
        // TODO
    }

    @Override
    public void enterAccessModifier(MiniJavaParser.AccessModifierContext ctx) {
        // TODO
    }

    @Override
    public void exitAccessModifier(MiniJavaParser.AccessModifierContext ctx) {
        // TODO
    }

    @Override
    public void enterNestedStatement(MiniJavaParser.NestedStatementContext ctx) {
        currentSymbolTable = SymbolTable.getSymbolTableByKey("nested_" + ctx.start.getLine() + "_" + ctx.start.getCharPositionInLine());
        System.out.println(currentSymbolTable);
        // TODO
    }

    @Override
    public void exitNestedStatement(MiniJavaParser.NestedStatementContext ctx) {
        currentSymbolTable = currentSymbolTable.getPreSymbolTable();
        // TODO
    }

    @Override
    public void enterIfElseStatement(MiniJavaParser.IfElseStatementContext ctx) {
        // TODO
    }

    @Override
    public void exitIfElseStatement(MiniJavaParser.IfElseStatementContext ctx) {
        // TODO
    }

    @Override
    public void enterWhileStatement(MiniJavaParser.WhileStatementContext ctx) {
        // TODO
    }

    @Override
    public void exitWhileStatement(MiniJavaParser.WhileStatementContext ctx) {
        // TODO
    }

    @Override
    public void enterPrintStatement(MiniJavaParser.PrintStatementContext ctx) {
        // TODO
    }

    @Override
    public void exitPrintStatement(MiniJavaParser.PrintStatementContext ctx) {
        // TODO
    }

    @Override
    public void enterVariableAssignmentStatement(MiniJavaParser.VariableAssignmentStatementContext ctx) {
        System.out.println(ctx.EQ().getText());
        for (int i=0; i<ctx.expression().size(); i++){
            System.out.println(ctx.expression(i).getText());
        }
        if (!getExpressionType(ctx.expression(0).getText()).equals(ctx.expression(1).getText())){
            errors.add(new Error(230,ctx.start.getLine(),ctx.start.getCharPositionInLine()," Incompatible types :" + getExpressionType(ctx.expression(0).getText()).toString() +"can not be converted to " + getExpressionType(ctx.expression(1).getText()).toString()));
        }


        // TODO
    }

    @Override
    public void exitVariableAssignmentStatement(MiniJavaParser.VariableAssignmentStatementContext ctx) {
        // TODO
    }

    @Override
    public void enterArrayAssignmentStatement(MiniJavaParser.ArrayAssignmentStatementContext ctx) {
        // TODO
        if (!getExpressionType(ctx.Identifier().getText()).equals(getExpressionType(ctx.expression().toString()))){
            errors.add(new Error(230,ctx.start.getLine(),ctx.start.getCharPositionInLine()," Incompatible types :" + getExpressionType(ctx.Identifier().getText()).toString() +"can not be converted to " + getExpressionType(ctx.expression().toString()).toString()));
        }
    }

    @Override
    public void exitArrayAssignmentStatement(MiniJavaParser.ArrayAssignmentStatementContext ctx) {
        // TODO
    }

    @Override
    public void enterLocalVarDeclaration(MiniJavaParser.LocalVarDeclarationContext ctx) {
        // TODO
    }

    @Override
    public void exitLocalVarDeclaration(MiniJavaParser.LocalVarDeclarationContext ctx) {
        // TODO
    }

    @Override
    public void enterExpressioncall(MiniJavaParser.ExpressioncallContext ctx) {
        // TODO
    }

    @Override
    public void exitExpressioncall(MiniJavaParser.ExpressioncallContext ctx) {
        // TODO
    }

    @Override
    public void enterIfBlock(MiniJavaParser.IfBlockContext ctx) {
        // TODO
    }

    @Override
    public void exitIfBlock(MiniJavaParser.IfBlockContext ctx) {
        // TODO
    }

    @Override
    public void enterElseBlock(MiniJavaParser.ElseBlockContext ctx) {
        // TODO
    }

    @Override
    public void exitElseBlock(MiniJavaParser.ElseBlockContext ctx) {
        // TODO
    }

    @Override
    public void enterWhileBlock(MiniJavaParser.WhileBlockContext ctx) {
        // TODO
    }

    @Override
    public void exitWhileBlock(MiniJavaParser.WhileBlockContext ctx) {
        // TODO
    }

    @Override
    public void enterLtExpression(MiniJavaParser.LtExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitLtExpression(MiniJavaParser.LtExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterObjectInstantiationExpression(MiniJavaParser.ObjectInstantiationExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitObjectInstantiationExpression(MiniJavaParser.ObjectInstantiationExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterArrayInstantiationExpression(MiniJavaParser.ArrayInstantiationExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitArrayInstantiationExpression(MiniJavaParser.ArrayInstantiationExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterPowExpression(MiniJavaParser.PowExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitPowExpression(MiniJavaParser.PowExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterIdentifierExpression(MiniJavaParser.IdentifierExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitIdentifierExpression(MiniJavaParser.IdentifierExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterMethodCallExpression(MiniJavaParser.MethodCallExpressionContext ctx) {
        int line = ctx.start.getLine();
        int column = ctx.start.getCharPositionInLine();

        if (currentSymbolTable.get("class_"+ctx.expression(0).getText()) == null && !ctx.expression(0).getText().equals("this")){
            errors.add(new Error(105,line,column,"cannot find class " + ctx.expression(0).getText()));
        }


        // TODO
    }

    @Override
    public void exitMethodCallExpression(MiniJavaParser.MethodCallExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterNotExpression(MiniJavaParser.NotExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitNotExpression(MiniJavaParser.NotExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterBooleanLitExpression(MiniJavaParser.BooleanLitExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitBooleanLitExpression(MiniJavaParser.BooleanLitExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterParenExpression(MiniJavaParser.ParenExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitParenExpression(MiniJavaParser.ParenExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterIntLitExpression(MiniJavaParser.IntLitExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitIntLitExpression(MiniJavaParser.IntLitExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterNullLitExpression(MiniJavaParser.NullLitExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitNullLitExpression(MiniJavaParser.NullLitExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterAndExpression(MiniJavaParser.AndExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitAndExpression(MiniJavaParser.AndExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterArrayAccessExpression(MiniJavaParser.ArrayAccessExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitArrayAccessExpression(MiniJavaParser.ArrayAccessExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterAddExpression(MiniJavaParser.AddExpressionContext ctx) {
        // TODO


    }

    @Override
    public void exitAddExpression(MiniJavaParser.AddExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterThisExpression(MiniJavaParser.ThisExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitThisExpression(MiniJavaParser.ThisExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterFieldCallExpression(MiniJavaParser.FieldCallExpressionContext ctx) {
        int line = ctx.start.getLine();
        int column = ctx.start.getCharPositionInLine();

        if (currentSymbolTable.get("class_"+ctx.expression().getText()) == null && !ctx.expression().getText().equals("this")){
            errors.add(new Error(105,line,column,"cannot find class " + ctx.expression().getText()));
        }
        // TODO
    }

    @Override
    public void exitFieldCallExpression(MiniJavaParser.FieldCallExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterArrayLengthExpression(MiniJavaParser.ArrayLengthExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitArrayLengthExpression(MiniJavaParser.ArrayLengthExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterIntarrayInstantiationExpression(MiniJavaParser.IntarrayInstantiationExpressionContext ctx) {
        // TODO
    }

    @Override
    public void exitIntarrayInstantiationExpression(MiniJavaParser.IntarrayInstantiationExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterSubExpression(MiniJavaParser.SubExpressionContext ctx) {


    }

    @Override
    public void exitSubExpression(MiniJavaParser.SubExpressionContext ctx) {
        // TODO
    }

    @Override
    public void enterMulExpression(MiniJavaParser.MulExpressionContext ctx) {

        // TODO
    }

    @Override
    public void exitMulExpression(MiniJavaParser.MulExpressionContext ctx) {
        // TODO
    }


    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        // TODO
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        // TODO
    }

    @Override
    public void visitTerminal(TerminalNode node) {
        // TODO
    }

    @Override
    public void visitErrorNode(ErrorNode node) {
        // TODO
    }

    public Type getExpressionType(String expression){

        if (expression.contains(".length")){
            return new IntType();
        }
        else if (expression.contains("new")){
            if (expression.contains("int")){
                return new IntType();
            }
            else if (expression.contains("bool")){
                return new BoolType();
            }
            else{
                MethodSymbolTableItem method = (MethodSymbolTableItem) currentSymbolTable.get("method_"+expression.split(" ")[1]);
                return method.getReturnType();
            }
        }
        else if (expression.contains("!") | expression.contains(">") | expression.contains("<") | expression.contains("&&") |expression.contains("true") |expression.contains("false") ){
            return new BoolType();
        }
        else if (expression.contains("*") | expression.contains("+") | expression.contains("/") | expression.contains("-")){
            return new IntType();
        }
        else if (expression.chars().allMatch( Character::isDigit )){
            return new IntType();
        }
        else{
            //TODO change this add array type
         return new ArrayType(new IntType());
        }
    }






}